import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class Core {
    private HashMap<Integer, Node> nodes;
    //private HashMap<Integer, Edge> edges;
    public Core() {
        this.nodes = new HashMap<>();
        //this.edges = new HashMap<Integer, Edge>();
    }

    public HashMap<Integer, Node> getNodes() {
        return this.nodes;
    }

    /*public HashMap<Integer, Edge> getEdges() {
        return this.edges;
    }*/

    public void addNode(Node n) {
        this.nodes.put(n.hashCode(), n);
    }

    public void addEdge(String label, Node s, Node e) {
        System.out.println("Label: " + label);
        System.out.println("Start: " + s);
        System.out.println("End: " + e);
        this.nodes.get(s.hashCode()).addEdge(label, e);
    }

    public void deleteNode(Integer i) {
        this.nodes.remove(i);
    }

    public void deleteEdge(String label, Node s, Node e) {
        this.nodes.get(s.hashCode()).deleteEdge(label, e);
    }

    public JSONObject toJSONObject() {
        JSONObject res = new JSONObject();
        JSONArray nodes = new JSONArray();
        for (Node n: this.nodes.values()) {
            nodes.put(n.toJSONObject());
        }
        res.put("nodes", nodes);
        return res;
    }
}