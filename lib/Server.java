import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.json.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.Executors;

public class Server {
    //private static Core core;

    public static void main(String[] args) throws IOException {
        Core core = new Core();
        Node a = new Node("A");
        Node b = new Node("B");
        core.addNode(a);
        core.addNode(b);
        core.addEdge("C", a, b);


        InetSocketAddress addr = new InetSocketAddress(3001);
        HttpServer server = HttpServer.create(addr, 0);

        server.createContext("/", exchange -> {
            String requestMethod = exchange.getRequestMethod();
            String requestURI = exchange.getRequestURI().toString();
            System.out.println(requestMethod + " " + requestURI);

            if (requestMethod.equalsIgnoreCase("POST")) {
                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.set("Content-Type", "text/json");
                exchange.sendResponseHeaders(200, 0);


                OutputStream responseBody = exchange.getResponseBody();
                InputStream requestBodyStream = exchange.getRequestBody();

                Scanner s = new Scanner(requestBodyStream, StandardCharsets.UTF_8.name()).useDelimiter("\\A");
                String requestBody = s.hasNext() ? s.next() : "";

                System.out.println("Request Body: " + requestBody);

                JSONObject requestJSON = new JSONObject();
                if (!requestBody.equals("")) {
                    requestJSON = new JSONObject(requestBody);
                }

                String responseData = "";

                if (requestURI.equals("/nodes")) {
                    JSONObject res = core.toJSONObject();
                    responseData = res.toString();

                } else if (requestURI.equals("/nodes/create")) {
                    String data = requestJSON.getString("data");
                    Node n = new Node(data);
                    core.addNode(n);

                    JSONObject res = new JSONObject();
                    res.put("node", n.toString());
                    responseData = res.toString();

                } else if (requestURI.equals("/nodes/delete")) {
                    Integer nodeid = requestJSON.getInt("nodeid");
                    core.deleteNode(nodeid);

                    JSONObject res = new JSONObject();
                    res.put("node", nodeid);
                    responseData = res.toString();

                } else if (requestURI.equals("/edges/create")) {
                    String label = requestJSON.getString("label");
                    String startId = requestJSON.getString("start");
                    String endId = requestJSON.getString("end");
                    System.out.println(startId);
                    System.out.println(endId);

                    Node start = core.getNodes().get(Integer.parseInt(startId));
                    Node end = core.getNodes().get(Integer.parseInt(endId));
                    if (start  != null && end != null) {
                        core.addEdge(label, start, end);
                    }

                    JSONObject res = new JSONObject();
                    responseData = res.toString();

                } else if (requestURI.equals("/edges/delete")) {
                    String label = requestJSON.getString("label");
                    String startId = requestJSON.getString("start");
                    String endId = requestJSON.getString("end");

                    Node start = core.getNodes().get(Integer.parseInt(startId));
                    Node end = core.getNodes().get(Integer.parseInt(endId));
                    if (start != null && end != null) {
                        core.deleteEdge(label, start, end);
                    }

                    JSONObject res = new JSONObject();
                    responseData = res.toString();

                } else if (requestURI.equals("/traversal")) {
                    String query = requestJSON.getString("query");
                    Traversal t = new Traversal(query, core);
                    String result = t.run();

                    JSONObject res = new JSONObject();
                    res.put("result", result);
                    responseData = res.toString();
                }

                System.out.println("Response Data: " + responseData);

                responseBody.write(responseData.getBytes());
                responseBody.close();
            }
        });
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();
        System.out.println("Server is listening on port 3001" );
    }
}