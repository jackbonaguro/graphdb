import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class Node {
    private String data;
    private HashMap<String, Collection<Node>> edges;

    public Node(String data) {
        this(data, new HashMap<>());
    }

    public Node(String data, HashMap<String, Collection<Node>> edges) {
        this.data = data;
        this.edges = edges;
    }

    public String getData() {
        return this.data;
    }

    public void addEdge(String label, Node neighbor) {
        Collection<Node> neighbors = this.edges.get(label);
        if (neighbors == null) {
            //First use of this label
            this.edges.put(label, new HashSet<>());
            this.edges.get(label).add(neighbor);
        } else {
            neighbors.add(neighbor);
        }
    }

    public void deleteEdge(String label, Node neighbor) {
        Collection<Node> neighbors = this.edges.get(label);
        if (neighbors == null) {
            //Label is unused
            return;
        } else {
            if (neighbors.contains(neighbor)) {
                neighbors.remove(neighbor);
            }
        }
    }

    public HashMap<String, Collection<Node>> getEdges() {
        return this.edges;
    }

    public JSONObject toJSONObject() {
        JSONObject res = new JSONObject();
        res.put("id", this.hashCode());
        res.put("data", this.data);
        JSONArray edges = new JSONArray();
        for (String label: this.edges.keySet()) {
            JSONObject list = new JSONObject();
            list.put("label", label);
            JSONArray neighborIds = new JSONArray();
            for (Node n: this.edges.get(label)) {
                neighborIds.put(n.hashCode());
            }
            list.put("neighbors", neighborIds);
            edges.put(list);
        }
        res.put("edges", edges);
        return res;
    }
}