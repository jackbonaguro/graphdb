import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class Traversal {
    private Core core;
    private String query;
    private String[] steps;
    private Node curr;
    private int index;

    public Traversal(String query, Core core) {
        this.query = query;
        this.core = core;

        this.steps = this.query.split("\\.");
        this.curr = null;
        this.index = 0;
    }

    public String run() {
        Iterator<Node> iterator = new Iterator<Node>() {
            @Override
            public boolean hasNext() {
                return (Traversal.this.index < Traversal.this.steps.length);
            }

            @Override
            public Node next() {
                Node n = Traversal.this.handleStep(Traversal.this.steps[index], Traversal.this.curr);
                Traversal.this.index++;
                return n;
            }
        };

        while (iterator.hasNext()) {
            curr = iterator.next();
        }
        return curr.getData();
    }

    private Node handleStep(String step, Node curr) {
        String[] parts = step.split("\\(");
        String method = parts[0];
        String arg = parts[1].substring(0, parts[1].length() - 1);

        switch (method) {
            case ("get"): {
                return this.core.getNodes().get(Integer.parseInt(arg));
            }
            case ("neighbor"): {
                HashMap<String, Collection<Node>> neighbors = curr.getEdges();
                return neighbors.get(arg).iterator().next();
            }
            default: {
                return null;
            }
        }
    }
}