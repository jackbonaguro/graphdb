var express = require('express');
var request = require('request');
var router = express.Router();

router.post('/nodes', (req, res) => {
   request.post('http://localhost:3001/nodes', {}, (err, response, body) => {
       console.log(body);
       res.send(body);
   });
});

router.post('/nodes/create', (req, res) => {
    request.post({
        uri: 'http://localhost:3001/nodes/create',
        body: JSON.stringify({data: req.body.data})
    }, (err, response, body) => {
        console.log(body);
        res.send(body);
    });
});

router.post('/nodes/delete', (req, res) => {
    request.post({
        uri: 'http://localhost:3001/nodes/delete',
        body: JSON.stringify({nodeid: req.body.nodeid})
    }, (err, response, body) => {
        console.log(body);
        res.send(body);
    });
});

router.post('/edges/create', (req, res) => {
    request.post({
        uri: 'http://localhost:3001/edges/create',
        body: JSON.stringify({
            label: req.body.label,
            start: req.body.start,
            end: req.body.end
        })
    }, (err, response, body) => {
        console.log(body);
        res.send(body);
    });
});

router.post('/edges/delete', (req, res) => {
    request.post({
        uri: 'http://localhost:3001/edges/delete',
        body: JSON.stringify({
            label: req.body.label,
            start: req.body.start,
            end: req.body.end
        })
    }, (err, response, body) => {
        console.log(body);
        res.send(body);
    });
});

router.post('/traversal', (req, res) => {
    request.post({
        uri: 'http://localhost:3001/traversal',
        body: JSON.stringify({
            query: req.body.query
        })
    }, (err, response, body) => {
        console.log(body);
        res.send(body);
    });
});

module.exports = router;
