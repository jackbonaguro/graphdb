# GraphDB

Proof of concept graph data model for highly relational datasets. The goal of this project is to eliminate polling behavior and conflicting records by allowing information to flow naturally across an application's entire domain.